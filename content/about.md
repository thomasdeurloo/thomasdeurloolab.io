+++
title = "about/"
date = "2014-04-09"
aliases = ["about", "about-us","about-me","contact"]
[ author ]
  name = "Thomas Deurloo"
+++

My name is Thomas Deurloo, I live in the Netherlands, and I work as a hydrologist at [waterschap Brabantse Delta](https://www.brabantsedelta.nl/), a waterboard in the mid-western region of North Brabant, based in Breda. There I work on keeping the water system, robust and climate proof. In my day-to-day work I work with python (sometimes R), GIS (both QGIS & ArcGIS), machine learning and physical models (mostly SOBEK).

Furthermore I'm a part-time freelance data scientist in the water sector. Please feel free to [contact me](mailto:thomas@thomasdeurloo.nl) if you are interested.

I'm a water manager by training. I hold a MSc degree in [Water Management](https://www.tudelft.nl/citg/over-faculteit/afdelingen/watermanagement/), track Water Resources from Delft University of Technology.

With my blog I hope to spread the data-related love in the Water sector. Some blogs will be written in Dutch others in English. Check out my latest [blog posts](https://thomas-wsbd.github.io/posts/). In the [gallery](https://thomas-wsbd.github.io/gallery/) you can find some of my spare time creations.