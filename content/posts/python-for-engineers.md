---
author:
  name: "Thomas Deurloo"
date: 2019-12-29
linktitle: Python for engineers
type:
- post
- posts
title: Python for engineers, the basics
weight: 10
tags:
- cursus
- python
- basis
series:
- other
---

## Python for engineers, een basis cursus met notebooks
Afgelopen maand het ik de eer gehad om mijn collega's mee te nemen in de wonderlijke wereld van python. In een 5-tal middagen hebben we de "basis" van python behandeld (ja dit is erg kort en de stof was eigenlijk teveel voor deze middagen). Hiervoor heb ik verschillende notebooks opgesteld (in het Nederlands) die de basis (wat mij betreft) voor het gebruik van python in een technische omgeving behandelen. We hebben het gehad over python (van operators tot de basis data structures), pandas & geopandas, APIs (met requests), plotten (met matplotlib en seaborn) en pastas (geohydrologische tijdreeksanalyse).

Altijd al eens python willen uitproberen? De notebooks (uitleg, voorbeelden en oefeningen) zijn terug te vinden op mijn [github](https://github.com/thomas-wsbd/python-course-wsbd). Succes en veel plezier met python. Ik zal in de loop van deze maand ook de oplossingen van de verschillende oefeningen op github posten.

Disclaimer: de notebooks zijn gemaakt voor en op de python omgeving van mijn werk, deze kan afwijken van jouw python omgeving hierdoor kan mogelijk sommige code niet direct werken. Als je vastloopt kan je me altijd benaderen.

PS.
Als je echt vanaf 0 begint, installeer [Anaconda](https://www.anaconda.com/distribution/), dit is een python distribution met de meest voorkomende packages reeds geinstalleerd (ik denk wel dat je voor geopandas nog iets zal moeten installeren, google dit even). Je kan de notebooks openen via jupyter notebooks uit het Anaconda menu, zorg er wel voor dat jupyter opent in de juiste map, of de bestanden naar de map waar jupyter opent verplaatst.