+++
title = "gallery/"
date = "2014-04-09"
aliases = ["gallery", "portfolio"]
[ author ]
  name = "Thomas Deurloo"

+++

&NewLine;
[![click me to open in full size](/static/img/gallery/climate-stripes.png)](/static/img/gallery/climate-stripes.png)

``#knmi`` ``#gilzerijen`` ``#climate``  ``#matplotlib`` 

&NewLine;
[![click me to open in full size](/static/img/gallery/zeespiegel.png)](/static/img/gallery/zeespiegel.png)

``#sealevelrise`` ``#quantiles`` ``#multiplestations``  ``#rwswaterinfo``  

&NewLine;  
[![click me to open in full size](/static/img/gallery/subbasins.png)](/static/img/gallery/subbasins.png)

``#subbasins`` ``#qgis`` ``#gdal``  ``#ahn3``

&NewLine;  
[![click me to open in full size](/static/img/gallery/rainradar.png)](/static/img/gallery/rainradar.png)

``#precipitationradar`` ``#contourplot`` ``#matplotlib``  ``#meteobase``
  
&NewLine;  
[![click me to open in full size](/static/img/gallery/rainworld.png)](/static/img/gallery/rainworld.png)  

``#precipitation`` ``#world`` ``#WorldClim``
  
&NewLine;  
[![click me to open in full size](/static/img/gallery/sealevel.png)](/static/img/gallery/sealevel.png)  

``#sealevelrise`` ``#boxplot`` ``#seaborn`` ``#savethepolarbears``  ``#rwswaterinfo``

&NewLine;  
[![click me to open in full size](/static/img/gallery/surfacetemperature.png)](/static/img/gallery/surfacetemperature.png)

``#satellitesurfacetemperature`` ``#contourplot`` ``#matplotlib`` ``#landsat8``
